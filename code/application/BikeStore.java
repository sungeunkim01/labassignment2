package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        //Hardcode the 4 bicycles
        bicycles[0] = new Bicycle("Bianchi", 25, 47.9);
        bicycles[1] = new Bicycle("Trek", 15, 34.8);
        bicycles[2] = new Bicycle("SCOTT", 23, 45.4);
        bicycles[3] = new Bicycle("BMC", 18, 40.6);
        
        //For each loop that through all 4 bycycles and print all of them by using toString().
        for (Bicycle bicycle: bicycles) {
            System.out.println(bicycle + "\n-----------------");
        }
    }
}
