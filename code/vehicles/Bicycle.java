package vehicles;

public class Bicycle {
    //Declare three private variables.
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //Constructor
    public Bicycle (String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    //Writing three getter method for the three private variables. 
    public String getManufacturer() {
        return manufacturer;
    }
    public int getNumberGears() {
        return numberGears;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }
    //Override method
    public String toString() {
        return "Manufacturer: " + manufacturer + "\n" + "Number of gears: " + numberGears + "\n" + "Max speed: " + maxSpeed;
    }
}